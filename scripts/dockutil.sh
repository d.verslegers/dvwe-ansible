@echo 'Cleaning and setting up dock'
dockutil --remove "Contacts" --allhomes
dockutil --remove "Safari"
dockutil --remove "Notes"
dockutil --remove "Reminders"
dockutil --remove "Maps"
dockutil --remove "FaceTime"
dockutil --remove "Photo Booth"
dockutil --remove "iTunes"
dockutil --remove "iBooks"
dockutil --remove "System Preferences"
dockutil --remove "Mail"
dockutil --remove "Calendar"
dockutil --remove "Messages"
dockutil --remove "Photos"
dockutil --remove "Siri"
dockutil --add /Applications/Launchpad.app --after 'Finder' --allhomes
dockutil --add /Applications/Canary Mail.app --after 'Launchpad' --allhomes
dockutil --add /Applications/MindNode.app --after 'Canary%20Mail' --allhomes
dockutil --add /Applications/Bitwarden.app --after 'MindNode' --allhomes
dockutil --add /Applications/iTerm.app --after 'Bitwarden' --allhomes
dockutil --add /Applications/Safari.app --after 'LastPass' --allhomes
dockutil --add /Applications/Firefox.app --after 'Safari' --allhomes
dockutil --add "/Applications/Google Chrome.app" --after 'Firefox' --allhomes
dockutil --add /Applications/Atom.app --after 'Google%20Chrome' --allhomes
dockutil --add /Applications/Pycharm.app --after 'Atom' --allhomes
dockutil --add /Applications/Tower.app --after 'Pycharm' --allhomes
dockutil --add '/Applications/Microsoft Remote Desktop Beta.app' --after 'System Preferences' --allhomes
